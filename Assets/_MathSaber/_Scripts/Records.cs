﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Record
{
    public string name;
    public int score;

    public Record(string name, int score)
    {
        this.name = name;
        this.score = score;
    }
}
[Serializable]
public struct ListRecords
{
    public List<Record> records;
    public ListRecords(List<Record> records)
    {
        this.records = records;
    }
}
