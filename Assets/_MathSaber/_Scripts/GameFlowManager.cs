﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFlowManager : MonoBehaviour
{
    public static GameFlowManager instance;
    /// <summary>
    /// Show Main Panel to select mode and start the game
    /// </summary>
    void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else Destroy(this);
        
    }

    void Start()
    {
        UIManager.instance.ShowMainPanel();
    }
    /// <summary>
    /// Reference to the Start Button in Main Panel
    /// </summary>
    public void StartGame()
    {
        UIManager.instance.StartGame();
    }

}
