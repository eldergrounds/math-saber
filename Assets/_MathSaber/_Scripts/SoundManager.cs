﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    [SerializeField]
    public AudioClip[] Music;

    public static SoundManager instance;
    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        if (!instance) instance = this;
        else Destroy(this);
    }
    public void PlayMusic(EnumMusic enumMusic)
    {
        audioSource.clip = Music[(int)enumMusic];
        audioSource.Play();
    }
}
