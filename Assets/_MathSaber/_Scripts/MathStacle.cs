﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

[RequireComponent(typeof(Rigidbody))]
public class MathStacle : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float life;
    [SerializeField] int score;
    [SerializeField] int damage;
    [SerializeField] Transform rotator;
    [SerializeField] List<Material> materials;
    [SerializeField] Renderer colorsRenderer;
    [SerializeField] TextMeshPro given;
    [SerializeField] TextMeshPro[] choices;
    [SerializeField] GameObject explosionGO;
    [SerializeField] GameObject flareGO;
    [SerializeField] TextMeshPro testText;
    private Rigidbody rigidbody;
    private float lifeAtStart;
    //public TextMeshPro result;

    private void Start()
    {
        lifeAtStart = life;
        rigidbody = GetComponent<Rigidbody>();
    }
    private void OnEnable()
    {
        Initialize();
    }

    void Initialize()
    {
        //var shuffledMaterials = materials.OrderBy(x => Random.value).ToList();
        //var rendererMaterials = colorsRenderer.materials;

        //for (int i = 0; i < 4; i++)
        //    rendererMaterials[i] = shuffledMaterials[i];

        //colorsRenderer.materials = rendererMaterials;
        
        GenerateGiven();
    }

    void GenerateGiven()
    {
        int x = Random.Range(1, 11);
        int y = Random.Range(1, 11);
        int answer = 0;
        switch (GameManager.mode)
        {
            case EnumMode.Addition:
                answer = x + y;
                given.text = x + " + " + y;
                break;
            case EnumMode.Subtraction:
                answer = x - y;
                given.text = x + " - " + y;
                break;
            case EnumMode.Division:
                answer = x / y;
                given.text = x + " / " + y;
                break;
            case EnumMode.Multiplication:
                answer = x * y;
                given.text = x + " x " + y;
                break;

        }
        choices[0].text = answer.ToString();

        for (int i = 1; i < choices.Length; i++)
        {
            int rand = Random.Range(1, 4);
            int[] mult = { -1, 1 };
            int multValue = mult[Random.Range(0, 2)];

            choices[i].text = (answer + (rand * multValue)).ToString();
        }
        rotator.Rotate(transform.forward, 90 * Random.Range(0, 4));
    }

    public void Hit(bool isCorrect)
    {
        UIManager.instance.UpdateScore(isCorrect ? score : -damage);
        ResetObject();
    }

    private void ResetObject(){
        life = lifeAtStart;
        gameObject.SetActive(false);
        transform.localPosition = Vector3.zero;
        life2test = 0;
    }
     
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag != "Wall") return;
        ResetObject();
        UIManager.instance.UpdateScore(Mathf.RoundToInt(-(damage / 2)));
    }
    // Update is called once per frame
    float life2test;
    void Update()
    {
        rigidbody.velocity = new Vector3(0, 0, -1 * speed);
        life -= Time.deltaTime;
        if(life <= 0)
        {
            ResetObject();
            UIManager.instance.UpdateScore(Mathf.RoundToInt(-(damage/2)));
        }

        life2test += Time.deltaTime;
        testText.text = life2test.ToString();
    }
}
