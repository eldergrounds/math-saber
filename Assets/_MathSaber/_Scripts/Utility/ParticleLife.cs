﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLife : MonoBehaviour
{
    [SerializeField] ParticleSystem particleSystem;

    private void OnEnable()
    {
        Invoke(nameof(Hide), particleSystem.main.duration);
    }
    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
