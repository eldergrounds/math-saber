﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class KeyboardEvent : UnityEvent<string>
{
}
public class Keyboard : MonoBehaviour
{
    public static KeyboardEvent keyboardEvent = new KeyboardEvent();

    [SerializeField] TMP_InputField input;
    [SerializeField] GameObject shiftUpButtonsPanel;
    [SerializeField] GameObject shiftDownButtonsPanel;

    public void ClickKey(string character)
    {
        input.text += character;
    }

    private void OnEnable()
    {
        shiftDownButtonsPanel.SetActive(true);
        shiftUpButtonsPanel.SetActive(false);
    }
    public void ShiftKey()
    {
        if (shiftUpButtonsPanel.activeSelf)
        {
            shiftDownButtonsPanel.SetActive(true);
            shiftUpButtonsPanel.SetActive(false);
        }
        else
        {
            shiftDownButtonsPanel.SetActive(false);
            shiftUpButtonsPanel.SetActive(true);
        }
    }

    public void Backspace()
    {
        if (input.text.Length > 0)
        {
            input.text = input.text.Substring(0, input.text.Length - 1);
        }
    }

    public void Enter()
    {
        //VRTK_Logger.Info("You've typed [" + input.text + "]");
        keyboardEvent.Invoke(input.text);
        input.text = "";
        gameObject.SetActive(false);
    }
}

