﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saber : MonoBehaviour
{
    public LayerMask layerMask;
    private Vector3 previousPos;
    private bool? isCorrect;

    RaycastHit hit;

    // Update is called once per frame
    void Update()
    {
        
        if(Physics.Raycast(transform.position,transform.forward, out hit, 1, layerMask))
        {
            if(Vector3.Angle(transform.position-previousPos, hit.transform.GetChild(0).up) > 130)
            {
                if (isCorrect == true) return;
                isCorrect = true;
                FXManager.instance.Emit(true, hit.transform.position);
                Debug.Log(hit.transform.parent.name + hit.transform.parent.position);
                hit.transform.GetComponent<MathStacle>().Hit(isCorrect:true);
            }
            else
            {
                if (isCorrect == false) return;
                isCorrect = false;
                FXManager.instance.Emit(false, hit.transform.position);
                Debug.Log(hit.transform.parent.name + hit.transform.position);
                hit.transform.GetComponent<MathStacle>().Hit(isCorrect: false);
            }
        }
        else
        {
            isCorrect = null;
        }
        previousPos = transform.position;
    }
}
