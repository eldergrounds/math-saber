﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class GameManager
{
    public static int m_score;
    public static EnumMode mode;
    public static float timer;
    public static List<Record> records = new List<Record>();
    public static string UpdateScore(int score)
    {
        if (score < 0) ResetCombo();
        m_score += score * multiplier;
        if (m_score < 0) m_score = 0;
        return m_score.ToString();
    }

    private const int Combo = 6;
    private static int hits;
    private static int multiplier = 1;
    public static string UpdateCombo()
    {
        hits++;
        if (hits >= Combo)
        {
            multiplier++;
        }
        return multiplier.ToString();
    }
    private static void ResetCombo()
    {
        hits = 0;
        multiplier = 1;
    }

    public static void Refresh()
    {
        m_score = 0;
        hits = 0;
        multiplier = 1;
    }

    public static void SaveScore(string name)
    {
        Debug.Log(m_score);
        records.Add(new Record(name, m_score));
        ListRecords listRecords = new ListRecords(records);
        try
        {
            string json = JsonUtility.ToJson(listRecords);
            Debug.Log(json);
            string filename = Application.persistentDataPath + "/" + mode + ".json";
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            File.WriteAllText(filename, json);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
        Refresh();


    }
}
