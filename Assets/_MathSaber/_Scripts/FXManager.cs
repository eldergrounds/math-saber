﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXManager : MonoBehaviour
{
    public static FXManager instance;

    void Awake()
    {
        if (!instance) instance = this;
        else Destroy(this);
    }

    public void Emit(bool isCorrect, Vector3 point)
    {
        Transform parent = isCorrect ? transform.GetChild(0) : transform.GetChild(1);
        foreach (Transform child in parent)
        {
            if (!child.gameObject.activeSelf)
            {
                child.position = point;
                child.gameObject.SetActive(true);
                return;
            }
        }
        Transform instans = Instantiate(parent.GetChild(0), Vector3.zero, Quaternion.identity);
        instans.SetParent(parent);
    }
}
