﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;

public class Scoreboard : MonoBehaviour
{
    List<Record> records = new List<Record>();
    [SerializeField] Transform contentLayout;
    [SerializeField] ScoreBar scorebarPrefab; 

    private void OnEnable()
    {
        UpdateScoreBoard();
    }

    public void UpdateScoreBoard()
    {
        GameManager.records.Clear();
        if (File.Exists(Application.persistentDataPath + "/" + GameManager.mode + ".json"))
        {
            string json = File.ReadAllText(Application.persistentDataPath + "/" + "Addition" + ".json");
            ListRecords listRecords = JsonUtility.FromJson<ListRecords>(json);
            Debug.Log(listRecords.records.Count);
            foreach (Record r in listRecords.records)
            {
                GameManager.records.Add(r);
            }
            GameManager.records = GameManager.records.OrderByDescending(s => s.score).ToList<Record>();
        }
        PopulateScore();
    }
    private void PopulateScore()
    {
        foreach (Transform child in contentLayout)
        {
            Destroy(child.gameObject);
        }
        foreach(Record r in GameManager.records)
        {
            ScoreBar sb = Instantiate<ScoreBar>(scorebarPrefab);
            sb.nameTMP.text = r.name;
            sb.scoreTMP.text = r.score.ToString();
            sb.transform.SetParent(contentLayout);
            RectTransform rec = sb.GetComponent<RectTransform>();
            rec.localPosition = Vector3.zero;
            rec.localRotation = Quaternion.identity;
        }
    }
       
}
