﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Instantiator : MonoBehaviour
{
    [SerializeField] Transform prefabTransform;
    [SerializeField] Transform[] point;
    [SerializeField] float beat;
    [SerializeField] float tempo;
    
    [SerializeField] int mathStaclesAmount;

    private float timer;
    private float frequency;
    void Awake()
    {
        frequency = tempo / (beat / 60) ;
        for (int i = 0; i < mathStaclesAmount; i++)
        {
            Instantiate();
        }
    }

    void Instantiate()
    {
        Transform instans = (Transform)Instantiate(prefabTransform, point[Random.Range(0,2)].position, Quaternion.identity);
        instans.SetParent(transform);
    }

    public void StartGame()
    {
        StartCoroutine(nameof(TimerStart));
        StartCoroutine(nameof(OnActivate));
    }

    IEnumerator OnActivate()
    {
        Invoke(nameof(PlayMusic), 5.6f);
        while (GameManager.timer > 0)
        {
            yield return new WaitForSeconds(frequency);
            Activate();
        }
    }
    void PlayMusic()
    {
        SoundManager.instance.PlayMusic(EnumMusic.Music_23787146);
    }
    IEnumerator TimerStart()
    {
        GameManager.timer = SoundManager.instance.Music[(int)EnumMusic.Music_23787146].length;
        //GameManager.timer = 15;
        while (GameManager.timer > 0)
        {
            GameManager.timer -= Time.deltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(5.6f);
        UIManager.instance.ShowResult();
    }

    void Activate()
    {
        foreach (Transform child in transform)
        {
            if (!child.gameObject.activeSelf)
            {
                child.position = point[Random.Range(0, 2)].position;
                child.gameObject.SetActive(true);
                return;
            }
        }
        Instantiate();
    }
}
