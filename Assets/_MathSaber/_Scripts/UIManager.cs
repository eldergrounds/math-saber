﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using VRTK;
public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField] Instantiator instantiator;

    [Header("__UI Canvas__")]
    [SerializeField] Canvas uICanvas;

    [Header("__Score Panel__")]
    [SerializeField] GameObject scorePanel;
    [SerializeField] TextMeshProUGUI scoreTMP;
    [SerializeField] TextMeshProUGUI comboTMP;

    [Header("__Main Panel__")]
    [SerializeField] GameObject mainPanel;
    [SerializeField] ToggleGroup toggleGroup;

    [Header("__Score Board Panel__")]
    [SerializeField] GameObject scoreboardPanel;

    [Header("__VRTK__")]
    [SerializeField] VRTK_Pointer [] VRTK_pointers;

    [Header("__Result Panel__")]
    [SerializeField] GameObject resultPanel;
    [SerializeField] TextMeshProUGUI resultNameTMP;
    [SerializeField] TextMeshProUGUI resultScoreTMP;
    [SerializeField] GameObject keyboard;
    [SerializeField] Button confirmButton;


    private void Awake()
    {
        if (!instance)
        {
            Keyboard.keyboardEvent.AddListener(OnKeyboardEnter);
            instance = this;
        }
        else Destroy(this);
    }

    public void ShowMainPanel()
    {
        foreach(Transform child in uICanvas.transform)
        {
            child.gameObject.SetActive(false);
        }
        mainPanel.SetActive(true);
        scoreboardPanel.SetActive(true);
    }

    public void OnChangeModeSelection()
    {
        foreach (Toggle toggle in toggleGroup.ActiveToggles())
        {
            if (toggle.isOn)
            {
                GameManager.mode = (EnumMode)Enum.Parse(typeof(EnumMode), toggle.name);
                break;
            }
        }
        Debug.Log(GameManager.mode);
        scoreboardPanel.GetComponent<Scoreboard>().UpdateScoreBoard();
    }
    public void StartGame()
    {
        foreach (Toggle toggle in toggleGroup.ActiveToggles())
        {
            if (toggle.isOn)
            {
                GameManager.mode = (EnumMode) Enum.Parse(typeof(EnumMode), toggle.name);
                break;
            }
        }
        foreach(VRTK_Pointer pointer in VRTK_pointers)
        {
            pointer.enabled = false;
        }
        instantiator.StartGame();
        mainPanel.SetActive(false);
        scoreboardPanel.SetActive(false);
        scorePanel.SetActive(true);
        scoreTMP.text = "0";
        comboTMP.text = "1";
        
    }

    public void UpdateScore(int score)
    {
        scoreTMP.text = GameManager.UpdateScore(score);
        comboTMP.text = GameManager.UpdateCombo() + "x";
    }

    public void ShowResult()
    {
        foreach (VRTK_Pointer pointer in VRTK_pointers)
        {
            pointer.enabled = true;
        }
        resultScoreTMP.text = scoreTMP.text;
        scorePanel.SetActive(false);
        resultPanel.SetActive(true);
        confirmButton.interactable = false;
        resultNameTMP.text = "";
    }

    public void OnEnterName()
    {
        keyboard.SetActive(true);
    }

    public void OnKeyboardEnter(string character)
    {
        resultNameTMP.text = character;
        if(character.Length > 0)
        {
            confirmButton.interactable = true;
        }
    }

    public void OnConfirmName()
    {
        GameManager.SaveScore(resultNameTMP.text);
        scoreboardPanel.GetComponent<Scoreboard>().UpdateScoreBoard();
        RestartGame();

    }
    
    void RestartGame()
    {
        mainPanel.SetActive(true);
        scoreboardPanel.SetActive(true);
        resultPanel.SetActive(false);
    }
}
